﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 10f;
    
    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(Vector3.up, rotationSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        float rotationPerFrameSpeed= Time.deltaTime * rotationSpeed;
        rotationPerFrameSpeed+= Time.deltaTime * rotationSpeed;
        transform.Rotate(Vector3.right, rotationPerFrameSpeed);
    }
}
