﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fuelBar : MonoBehaviour
{
    [SerializeField] float initialFuel = 1000f;
    private Vector3 currentFuelBarScale;
    private float currentFuel;
    

    // Start is called before the first frame update
    void Start()
    {
        currentFuelBarScale = transform.localScale;
        currentFuel = initialFuel;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            currentFuel -= Time.deltaTime;

            currentFuelBarScale.x = currentFuelBarScale.x * currentFuel / initialFuel;
            transform.localScale = currentFuelBarScale;
        }
    }
}


