﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightOscillator : MonoBehaviour
{
    [SerializeField] float initialIntensity = 1f;
    [SerializeField] float frequency = 1f;
    Light lightComponent;
    // Start is called before the first frame update
    void Start()
    {
        lightComponent = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        lightComponent.intensity = initialIntensity* Mathf.Sin(Time.time * Mathf.PI*2 * frequency/10);
    }
}
