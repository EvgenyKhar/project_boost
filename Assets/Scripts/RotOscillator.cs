﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotOscillator : MonoBehaviour
{
    private const float tau = Mathf.PI * 2;
    [Range(0,1)] [SerializeField] float rotationAmplitude =1f;
    [SerializeField] float frequency=1f;
    [SerializeField] Vector3 rotationVector = new Vector3(0f,0f,1f);
    
    private float cycle;
    private Vector3 initialRotation;

    private Vector3 offset;



    // Start is called before the first frame update
    void Start()
    {
        initialRotation = transform.eulerAngles;
 
    }

    // Update is called once per frame
    void Update()
    {
        cycle = Time.time * frequency / 10;
        offset = rotationAmplitude*rotationVector * Mathf.Sin(tau * cycle);
        transform.eulerAngles =initialRotation + offset;
        
    }
}
