﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    enum MovementType
    {
        Rotation,
        Thrusting,
        ThrustingAndRotating,
        None
    }

    enum MicroActionType
    {
        StartRotation,
        StopRotation,
        StartThrusting,
        StopThrusting
    }

    enum GameState
    {
        Alive,
        Dying,
        Transcending
    }

    Rigidbody rigidBody;
    AudioSource audioSource;
    [SerializeField] float m_rotationSpeed=150f;
    [SerializeField] float m_thrustingSpeed=1000f;
    [SerializeField] float sydeBoosterVolume = 0.3f;

    [SerializeField] AudioClip mainEngineSound;
    [SerializeField] AudioClip winLevelSound;
    [SerializeField] AudioClip deathSound;

    [SerializeField] ParticleSystem mainEngineParticles;
    [SerializeField] ParticleSystem winLevelParticles;
    [SerializeField] ParticleSystem deathParticles;

    MovementType currentMovementType = MovementType.None;
    GameState currentGameState = GameState.Alive;
    private float loadTime= 2f;
    public int currentLevelNumber;
    public bool isCollisionsOn= true;
    public float currentFuel= 1000f;


    // Start is called before the first frame update

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();

        audioSource.pitch = 0.8f;

        currentLevelNumber = SceneManager.GetActiveScene().buildIndex;

    }

    // Update is called once per frame
    void Update()
    {
        
        if (currentGameState == GameState.Alive & currentFuel>0)
        {
            RespondToRotateInput();
            RespondToThrustInput();
        }

        if (Debug.isDebugBuild)
        {
            ResponToDebugKeys();
        }
    }

    private void ResponToDebugKeys()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
            isCollisionsOn = !isCollisionsOn;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (currentGameState != GameState.Alive ) { return; }

            switch (collision.gameObject.tag)
            {
            
                case "Friendly":

                    break;
                case "Finish":
                    FinishingLevel();

                    break;
                case "Fuel":
                    print("+10 fuel!");
                    break;
                default:
                if(isCollisionsOn)
                {
                    Dying();
                }
                    break;
            }
        
    }

    private void FinishingLevel()
    {
        currentGameState = GameState.Transcending;
        currentMovementType = MovementType.None;
        SwitchSoundEffect(currentMovementType);
        audioSource.volume = 1.0f;
        audioSource.PlayOneShot(winLevelSound);
        mainEngineParticles.Stop();
        winLevelParticles.Play();
        Invoke("LoadNextLevel", loadTime);
    }

    private void Dying()
    {
        currentGameState = GameState.Dying;
        currentMovementType = MovementType.None;
        SwitchSoundEffect(currentMovementType);
        audioSource.volume = 1.0f;
        audioSource.PlayOneShot(deathSound);
        deathParticles.Play();
        Invoke("LoadLevel1", loadTime);
    }

    private void LoadLevel1()
    {
        SceneManager.LoadScene(0);
    }

    private void LoadNextLevel()
    {
        currentLevelNumber++;
        if(currentLevelNumber>=SceneManager.sceneCountInBuildSettings)
        {
            currentLevelNumber = 0;
        }
        SceneManager.LoadScene(currentLevelNumber);

    }

    private void RespondToRotateInput()
    {
        rigidBody.freezeRotation = true; //take manual control of rotation
        float rotationPerFrameSpeed = m_rotationSpeed * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
        {
            
            transform.Rotate(Vector3.forward, rotationPerFrameSpeed);

            SwitchCurrentMode(currentMovementType, MicroActionType.StartRotation);
            SwitchSoundEffect(currentMovementType);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(-Vector3.forward, rotationPerFrameSpeed);
            
            SwitchCurrentMode(currentMovementType, MicroActionType.StartRotation);
            SwitchSoundEffect(currentMovementType);
        }
        else
        {
            SwitchCurrentMode(currentMovementType, MicroActionType.StopRotation);
            SwitchSoundEffect(currentMovementType);
        }


        rigidBody.freezeRotation = false; //resume physics control of rotation
    }
    private void RespondToThrustInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            float thrustingPerFrameSpeed = Time.deltaTime * m_thrustingSpeed;
            rigidBody.AddRelativeForce(Vector3.up * thrustingPerFrameSpeed);
            SwitchCurrentMode(currentMovementType, MicroActionType.StartThrusting);
            SwitchSoundEffect(currentMovementType);
            mainEngineParticles.Play();
            currentFuel -= Time.deltaTime;
        }
        else
        {
            SwitchCurrentMode(currentMovementType, MicroActionType.StopThrusting);
            SwitchSoundEffect(currentMovementType);
            mainEngineParticles.Stop();
        }
    }
    private void SwitchCurrentMode(MovementType currentMovementType, MicroActionType action)
    {
        if (action == MicroActionType.StartRotation)
        {
            switch (currentMovementType)
            {
                case MovementType.None:
                    this.currentMovementType = MovementType.Rotation;
                    break;
                case MovementType.Thrusting:
                    this.currentMovementType = MovementType.ThrustingAndRotating;
                    break;
            }
        }
        else if (action == MicroActionType.StopRotation)
        {
            switch (currentMovementType)
            {
                case MovementType.ThrustingAndRotating:
                    this.currentMovementType = MovementType.Thrusting;
                    break;
                case MovementType.Rotation:
                    this.currentMovementType = MovementType.None;
                    break;
            }
        }
        else if (action == MicroActionType.StartThrusting)
        {
            switch (currentMovementType)
            {
                case MovementType.None:
                    this.currentMovementType = MovementType.Thrusting;
                    break;
                case MovementType.Rotation:
                    this.currentMovementType = MovementType.ThrustingAndRotating;
                    break;
            }
        }
        else if (action == MicroActionType.StopThrusting)
        {
            switch (currentMovementType)
            {
                case MovementType.Thrusting:
                    this.currentMovementType = MovementType.None;
                    break;
                case MovementType.ThrustingAndRotating:
                    this.currentMovementType = MovementType.Rotation;
                    break;
            }
        }
    }
    private void SwitchSoundEffect(MovementType movementType)
    {
        switch(movementType)
        {
            case MovementType.Rotation:
                if (!audioSource.isPlaying)
                {
                    audioSource.pitch = 1.5f;
                    audioSource.volume = sydeBoosterVolume;
                    audioSource.PlayOneShot(mainEngineSound);
                }
                else
                {
                    audioSource.volume = sydeBoosterVolume;
                    audioSource.pitch = 1.5f;
                }
                break;

            case MovementType.Thrusting:
            case MovementType.ThrustingAndRotating:

                if (!audioSource.isPlaying)
                {
                    audioSource.pitch = 1.0f;
                    audioSource.volume = 0.5f;
                    audioSource.PlayOneShot(mainEngineSound);
                }
                else
                {
                    audioSource.pitch = 1.0f;
                    audioSource.volume = 0.5f;
                }
                break;

            case MovementType.None:
                if (audioSource.isPlaying)
                {
                    audioSource.volume = 0;
                    audioSource.Stop();
                }
                break;
        } 
        
        
    }
}
