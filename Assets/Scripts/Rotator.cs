﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] int m_RotationSpeed=50;
    [SerializeField] Vector3 rotationDestination = Vector3.right;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        float rotationPerFrameSpeed = Time.deltaTime * m_RotationSpeed;
        transform.Rotate(rotationDestination, rotationPerFrameSpeed);
    }
}
