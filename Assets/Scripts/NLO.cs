﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NLO : MonoBehaviour
{
    [SerializeField] AudioClip audioClip;
    [SerializeField] float rangeFromLevel;

    AudioSource audioSource;
    

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if( Mathf.Abs(transform.position.x) < rangeFromLevel & !audioSource.isPlaying)
        {
            audioSource.PlayOneShot(audioClip);
        }
        else if(Mathf.Abs(transform.position.x) > rangeFromLevel & audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }
}
